// Hover navigation
const headerNavList = document.querySelectorAll(".header__nav-item");

headerNavList.forEach((headerNavItem) => {
    const navItem = headerNavItem.querySelector(".nav__item");
    headerNavItem.addEventListener("mouseover", () => {
        navItem.classList.add("nav__In");
    });

    headerNavItem.addEventListener("mouseout", () => {
        navItem.classList.remove("nav__In");
    });
});

// Hover sub navigation
const accountsList = document.querySelectorAll(".account__item");

accountsList.forEach((accountItem) => {
    const accountSubNav = accountItem.querySelector(".account__subNav");
    accountItem.addEventListener("mouseover", () => {
        accountSubNav.classList.add("subNav__In");
    });
    accountItem.addEventListener("mouseout", () => {
        accountSubNav.classList.remove("subNav__In");
    });
});

// Click Nav bar
const btnSideBar = document.getElementById("btnSideBar");
const btnClose = document.getElementById("btnClose");
const navBar = document.querySelector(".header__navBar");
let isActive = !navBar.classList.contains("none");

btnSideBar.addEventListener("click", () => {
    navBar.classList.remove("none");
    document.body.style.overflowY = "hidden";
    isActive = !isActive;
});

btnClose.addEventListener("click", () => {
    navBar.classList.add("none");
    document.body.style.overflowY = "visible";
    isActive = !isActive;
});

// Click Nav Account
const navAccountList = document.querySelectorAll(".nav__account-item");
const navAccountSubList = document.querySelectorAll(".nav__account-sub");
navAccountList.forEach((navAccountItem) => {
    navAccountItem.addEventListener("click", () => {
        const subMenu = navAccountItem.nextElementSibling;
        if (subMenu && subMenu.classList.contains("nav__account-sub")) {
            navAccountSubList.forEach((sub) => {
                if (sub !== subMenu) {
                    sub.classList.add("none");
                }
            });
            subMenu.classList.toggle("none");
        }
    });
});

window.addEventListener("resize", () => {
    const windowWidth = document.body.clientWidth;
    if (isActive) {
        if (windowWidth > 990) {
            navBar.classList.add("none");
        } else {
            navBar.classList.remove("none");
        }
    }
});
