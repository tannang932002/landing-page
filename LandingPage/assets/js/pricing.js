const inputCheck = document.querySelector("#switch");
const pricingAmount = document.querySelector(".price__number");

function countUp(upTo, num, countElement) {
    const delayTime = 200 / (num - upTo);
    if (upTo <= num) {
        countElement.innerHTML = upTo;
        setTimeout(() => {
            countUp(++upTo, num, countElement);
        }, delayTime);
    }
}

function countDown(downTo, num, countElement) {
    if (downTo >= num) {
        countElement.innerHTML = downTo;
        const delayTime = 200 / (downTo - num);
        setTimeout(() => {
            countDown(--downTo, num, countElement);
        }, delayTime);
    }
}

inputCheck.addEventListener("change", () => {
    if (inputCheck.checked) {
        countUp(pricingAmount.innerHTML, 49, pricingAmount);
    } else {
        countDown(49, 29, pricingAmount);
    }
});
