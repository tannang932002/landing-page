const slider = document.querySelector(".slider__container");
const slides = slider.querySelector(".slides");
const elements = Array.from(slides.children);

const prevButton = slider.querySelector("#prev");
const nextButton = slider.querySelector("#next");

const first = slides.firstElementChild;
const last = slides.lastElementChild;

const imageSlider = document.querySelector(".slider__list-img");
const image = [
    "https://landkit.goodthemes.co/assets/img/photos/photo-1.jpg",
    "https://landkit.goodthemes.co/assets/img/photos/photo-26.jpg",
];

let index = 1;
let offset = 0;
let dragStart = null;

cloneElement = (element, refElement) => {
    const clone = element.cloneNode(true);
    slides.insertBefore(clone, refElement);
    return clone;
};

disableNativeDragging = (element) => {
    element.draggable = false;
};

withTransitionSuspended = (callback) => {
    let scheduled = null;

    return () => {
        if (scheduled) {
            window.cancelAnimationFrame(scheduled);
        }

        scheduled = window.requestAnimationFrame(() => {
            slides.style.transition = "none";
            callback();

            scheduled = window.requestAnimationFrame(() => {
                slides.style.transition = "";
                scheduled = null;
            });
        });
    };
};

toggleDisabled = (disabled) => {
    prevButton.disabled = nextButton.disabled = disabled;
};

getOffset = () => {
    return elements
        .slice(0, index)
        .reduce((width, element) => width + element.clientWidth, 0);
};

translateSlides = (deltaX = 0) => {
    slides.style.transform = `translateX(${-offset + deltaX}px)`;
};

shiftSlides = (newIndex = index) => {
    index = newIndex;
    offset = getOffset();
    translateSlides();
    if (index === 2) {
        imageSlider["src"] = image[1];
    } else {
        imageSlider["src"] = image[0];
    }
};

fakeInfinity = () => {
    switch (index) {
        case 0:
            return shiftSlides(elements.length - 2);
        case elements.length - 1:
            return shiftSlides(1);
        default:
    }
};

startDragging = (event) => {
    slides.style.transition = "none";
    dragStart = event.clientX;
    toggleDisabled(true);
};

doDragging = (event) => {
    if (dragStart === null) {
        return;
    }

    translateSlides(event.clientX - dragStart);
};

stopDragging = (event) => {
    if (dragStart === null) {
        return;
    }

    slides.style.transition = "";
    shiftSlides(index + (event.clientX < dragStart ? 1 : -1));
    dragStart = null;
};

elements.push(cloneElement(first, null));
elements.unshift(cloneElement(last, first));

[slides, ...elements].forEach(disableNativeDragging);

prevButton.addEventListener("click", () => shiftSlides(index - 1));
nextButton.addEventListener("click", () => shiftSlides(index + 1));

slides.addEventListener("mousedown", startDragging);
slides.addEventListener("mousemove", doDragging);
slides.addEventListener("mouseleave", stopDragging);
slides.addEventListener("mouseup", stopDragging);

slides.addEventListener("transitionstart", () => toggleDisabled(true));
slides.addEventListener("transitionend", () => toggleDisabled(false));
slides.addEventListener("transitionend", withTransitionSuspended(fakeInfinity));
window.addEventListener("resize", withTransitionSuspended(shiftSlides));
window.dispatchEvent(new Event("resize"));
