function Validator(formSelector) {
    var formRules = {};
    var _this = this;

    function getParentElement(element, selector) {
        while (element.parentElement) {
            if (element.parentElement.matches(selector)) {
                return element.parentElement;
            } else {
                element = element.parentElement;
            }
        }
    }

    var validatorRules = {
        required: (value) => {
            return value ? undefined : "Vui lòng nhập trường này!";
        },
        email: (value) => {
            const regex =
                /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
            return regex.test(value) ? undefined : "Email không hợp lệ";
        },
        password: (value) => {
            return value.length >= 8 ? undefined : "Mật khẩu tối thiểu 8 kí tự";
        },
    };

    var formElement = document.querySelector(formSelector);
    if (formElement) {
        var inputs = formElement.querySelectorAll("[name][rules]");
        for (var input of inputs) {
            var rules = input.getAttribute("rules").split("|");
            for (var rule of rules) {
                if (Array.isArray(formRules[input.name])) {
                    formRules[input.name].push(validatorRules[rule]);
                } else {
                    formRules[input.name] = [validatorRules[rule]];
                }
            }
            input.onblur = handleValidate;
            input.oninput = handleClearError;
        }

        function handleValidate(event) {
            var rules = formRules[event.target.name];
            var errorMessage;
            for (var rule of rules) {
                errorMessage = rule(event.target.value);
                if (errorMessage) break;
            }

            if (errorMessage) {
                var formGroup = getParentElement(
                    event.target,
                    ".form__floating"
                );
                if (formGroup) {
                    formGroup.classList.add("invalid");
                    var formMessage = formGroup.querySelector(".form__error");
                    if (formMessage) {
                        formMessage.innerText = errorMessage;
                    }
                }
            }
            return !errorMessage;
        }

        function handleClearError(event) {
            var formGroup = getParentElement(event.target, ".form__floating");
            if (formGroup.classList.contains("invalid")) {
                formGroup.classList.remove("invalid");
                var formMessage = formGroup.querySelector(".form__error");
                if (formMessage) {
                    formMessage.innerText = "";
                }
            }
        }

        formElement.onsubmit = function (event) {
            event.preventDefault();

            var inputs = formElement.querySelectorAll("[name][rules]");
            var isValid = true;
            for (var input of inputs) {
                if (!handleValidate({ target: input })) {
                    isValid = false;
                }
            }

            if (isValid) {
                if (typeof _this.onSubmit === "function") {
                    _this.onSubmit();
                } else {
                    inputs.forEach((input) => {
                        console.log(`${input.name}: ${input.value}`);
                    });
                }
            }
        };
    }
}
